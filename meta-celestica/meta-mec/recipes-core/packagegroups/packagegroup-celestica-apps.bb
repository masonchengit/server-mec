#"Copyright ? 2020-present Celestica
#Licensed under BSD-3, see COPYING.BSD file for details."

SUMMARY = "OpenBMC for Celestica - Applications"
PR = "r1"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = " \
        ${PN}-chassis \
        ${PN}-fans \
        ${PN}-flash \
        ${PN}-system \
        "

PROVIDES += "virtual/obmc-chassis-mgmt"
PROVIDES += "virtual/obmc-fan-mgmt"
PROVIDES += "virtual/obmc-flash-mgmt"
PROVIDES += "virtual/obmc-system-mgmt"

RPROVIDES_${PN}-chassis += "virtual-obmc-chassis-mgmt"
RPROVIDES_${PN}-fans += "virtual-obmc-fan-mgmt"
RPROVIDES_${PN}-flash += "virtual-obmc-flash-mgmt"
RPROVIDES_${PN}-system += "virtual-obmc-system-mgmt"

SUMMARY_${PN}-chassis = "Celestica Chassis"
RDEPENDS_${PN}-chassis = " \
        x86-power-control \
        obmc-host-failure-reboots \
        "

SUMMARY_${PN}-fans = "Celestica Fans"
RDEPENDS_${PN}-fans = ""

SUMMARY_${PN}-flash = "Celestica Flash"
RDEPENDS_${PN}-flash = " \
        obmc-control-bmc \
        phosphor-software-manager \
        "

SUMMARY_${PN}-system = "Celestica System"
RDEPENDS_${PN}-system = " \
        ipmitool \
        tree \
        bmcweb \
        dbus-sensors \
        entity-manager \
        phosphor-webui \
        "
