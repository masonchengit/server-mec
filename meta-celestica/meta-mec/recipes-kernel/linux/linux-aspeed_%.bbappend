#"Copyright (c) 2020-present Celestica
#Licensed under BSD-3, see COPYING.BSD file for details."

FILESEXTRAPATHS_prepend_mec := "${THISDIR}/${PN}:"
SRC_URI_append_mec = " file://mec.cfg \
                        file://0001-linux-aspeed.patch \
                        file://0002-linux-aspeed-gpio-i2c.patch \
                        "
