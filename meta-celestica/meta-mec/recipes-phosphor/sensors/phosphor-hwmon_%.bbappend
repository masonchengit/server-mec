#"Copyright (c) 2020-present Celestica
#Licensed under BSD-3, see COPYING.BSD file for details."

FILESEXTRAPATHS_prepend_mec := "${THISDIR}/${PN}:"
EXTRA_OECONF_append_mec = " --enable-negative-errno-on-fail"

CHIPS = " \
        bus@1e78a000/i2c-bus@100/tmp75@48 \
        bus@1e78a000/i2c-bus@100/tmp75@4a \
        "
ITEMSFMT = "ahb/apb/{0}.conf"

ITEMS = "${@compose_list(d, 'ITEMSFMT', 'CHIPS')}"
#ITEMS += "iio-hwmon.conf"

ENVS = "obmc/hwmon/{0}"
SYSTEMD_ENVIRONMENT_FILE_${PN}_append_mec := "${@compose_list(d, 'ENVS', 'ITEMS')}"
